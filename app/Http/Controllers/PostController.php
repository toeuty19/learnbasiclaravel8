<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Post;

class PostController extends Controller
{
    //
    public function all_posts()
    {
        $datas = DB::table('posts')->get();
        //return dd($datas);
        return view('posts',compact('datas'));
    }
    public function add_post()
    {
        return view('add-post');
    }

    public function post_edit($id)
    {
        $datas = DB::table('posts')->where('id',$id)->first();
        //return dd($datas);
        return view('edit',compact('datas'));
    }
    public function add_post_submit(Request $request)
    {
        DB::table('posts')->insert([
            'tittle' => $request -> tittle,
            'description' => $request -> description
        ]);
        return back()->with('CREATED_SUCCESS','your data has been save to database successed.');
    }

    public function post_deleted($id)
    {
        DB::table('posts')->where('id',$id)->delete();
        return back()->with('deleted','data delete successed.');
    }

    public function post_update(Request $request)
    {
        DB::table('posts')->where('id',$request)->update([
            'tittle' => $request->tittle,
            'description' => $request->description
        ]);
        return back()->with('updated','data has been successfully.');
    }
    //-----------------------learn cluse inner join-----------------.

    public function innerjoin_table()
    {
        $request = DB::table('posts')->join('users','users.userId','=','posts.userId')
                                     ->select('posts.id','posts.tittle','posts.description','users.fullName')
                                     ->get();
        //return dd($request);
        return view('join_table',['request'=>$request]);
    }

    public function all_post_model()
    {
        $posts = Post::all();
        return $posts;
    }
}
