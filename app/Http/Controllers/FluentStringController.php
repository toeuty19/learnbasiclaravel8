<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FluentStringController extends Controller
{
    //
    public function index()
    {
        $lower = Str::of('This is lower character')->lower();
        echo $lower;

        echo '<br/>--------------------------------------------<br/>';
        $upper = Str::of('this is upper character.')->upper();
        echo $upper;

        echo '<br/>--------------------------------------------<br/>';
        $tittle = Str::of('this is convert to title')->title();
        echo $tittle;

        echo '<br/>--------------------------------------------<br/>';
        $slice = Str::of('wellcome to my youtube chanennel')->after('wellcome to');
        echo $slice;

        echo '<br/>--------------------------------------------<br/>';
        $slice1 = Str::of('App\Http\Controllers\Controller')->afterLast('\\');
        echo $slice1;

        echo '<br/>--------------------------------------------<br/>';
        $slice2 = Str::of('App\Http\Controllers\Controller')->append('s');
        echo $slice2;

        echo '<br/>--------------------------------------------<br/>';
        $slice3 = Str::of('App\Http\Controllers\Controller')->replace('Controller','stringController');
        echo $slice3;

        echo '<br/>--------------------------------------------<br/>';
        $slug = Str::of('Laravel Framwork')->slug(' - ');
        echo $slug;

        echo '<br/>--------------------------------------------<br/>';
        $sbstr = Str::of('Laravel 8 Framwork')->substr(10,8);
        echo $sbstr;

        echo '<br/>--------------------------------------------<br/>';
        $trim = Str::of('s Laravel')->trim('s');
        echo $trim;
        
    }
}
