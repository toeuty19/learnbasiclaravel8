<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function getSessionData(Request $request)
    {
        if($request->session()->has('name'))
        {
            echo $request->session()->get('name');
        }
        else
        {
            echo 'data not found';
        }
    }

    public function setSessionData(Request $request)
    {
        $request->session()->put('name','toeu ty');
        echo 'session has been set ';
    }

    public function deleteSession(Request $request)
    {
        $request->session()->forget('name');
        echo 'session has been remove';
    }
}
