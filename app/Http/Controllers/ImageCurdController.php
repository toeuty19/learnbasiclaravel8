<?php

namespace App\Http\Controllers;

use App\Models\ImageCurd;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;

class ImageCurdController extends Controller
{
    //
    public function view_curd_image()
    {
        return view('curd_image.add_new_image');
    }
    public function add_new_image(Request $request)
    {
        $image = $request->file('file');
        $name = $request->name;
        $imageName = time() .'.'. $image->extension();
        $image->move(public_path('images'), $imageName);

        $curd = new ImageCurd();
        $curd->name = $name;
        $curd->imageName = $imageName;
        $curd->save();
        return back()->with('created_image','Image has been created successfully.');
    }

    public function select_view_all_image()
    {
        $curds = ImageCurd::orderBy('id','DESC')->get();
        return view('curd_image.view_all_image',compact('curds'));
    }

    public function edit($id)
    {
        $curd = ImageCurd::find($id);
        return view('curd_image.update_image',compact('curd'));
    }

    public function update(Request $request)
    {
        $curd = ImageCurd::find($request->id);
        if($request->hasFile('file'))
        {
            unlink(public_path('images') . '/' . $curd->imageName);
            $image = $request->file('file');
            $imageName = time() . '.' . $image->extension();
            $image->move(public_path('images'), $imageName);
            $curd->imageName = $imageName;
        }
        
        $curd->name = $request->name;
        $curd ->save();

        return redirect('/select_view_all_image')->with('updated_image','Images has been updated successfully.');
    }

    public function deleted_image($id)
    {
        $curd = ImageCurd::find($id);
        unlink(public_path('images').'/'.$curd->imageName);
        $curd->delete();
        return redirect('/select_view_all_image')->with('deleted_image','Image has been deleted successfully.');
    }
}
