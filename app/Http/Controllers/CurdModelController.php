<?php

namespace App\Http\Controllers;

use App\Models\CurdModel;
use Illuminate\Http\Request;
use Reflector;

class CurdModelController extends Controller
{
    //
    public function view_index()
    {
        return view('curd.curd_view');
    }

    public function add_new(Request $resquest)
    {

        $resquest ->validate([
            'name'      =>'required|max:255',
            'gender'    =>'required|max:2',
            'date'      =>'required|max:30',
            'address'   =>'required|max:500',
            'phone'     =>'required|max:15',
            'email'     => 'required|email'
        ]);

        $curd = new CurdModel();
        $curd->name = $resquest ->name;
        $curd->gender = $resquest ->gender;
        $curd->dob = $resquest ->date;
        $curd->address = $resquest ->address;
        $curd->phone = $resquest->phone;
        $curd->email = $resquest->email;

        $curd->save();

        return redirect ('list-curd')->with('create_curd','One record has been created.');
    }

    public function list_curd()
    {
        $curds = CurdModel::orderBy('id','DESC')->get();
        //return dd($curd);
        return view('curd.view_list',compact('curds'));  
    }

    public function edit($id)
    {
        $curd = CurdModel::where('id',$id)->first();
        return view('curd.update',compact('curd'));
    }

    public function update(Request $resquest)
    {

        $resquest->validate([
            'name'      => 'required|max:255',
            'gender'    => 'required|max:2',
            'date'      => 'required|max:30',
            'address'   => 'required|max:500',
            'phone'     => 'required|max:15',
            'email'     => 'required|email'
        ]);
        $curd = CurdModel::find($resquest->id);
        $curd->name = $resquest->name;
        $curd->gender = $resquest->gender;
        $curd->dob = $resquest->date;
        $curd->address = $resquest->address;
        $curd->phone = $resquest->phone;
        $curd->email = $resquest->email;
        
        $curd->update();

        return redirect('list-curd')->with('updated','Record has been updated successed');
    }

    public function deletecurd($id)
    {
        $curd = CurdModel::find($id)->delete();
        return back()->with('deleted','One record has bee deleted.');
    }
}
