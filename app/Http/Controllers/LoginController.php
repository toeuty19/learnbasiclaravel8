<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    //
    public function index()
    {
        return view('User.login');
    }
    public function Login(Request $resquest)
    {
        $resquest->validate(['email'=> 'required|email','password'=>'required|min:6|max:12']);

        $email = $resquest->input('email');
        $password = $resquest->input('password');

        return 'Email : '.Str::of($email)->lower().' password : '.$password;
    }
}
