<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewController extends Controller
{
    //
    public function index()
    {
        $name = 'cheng horn';
        $views = array(
            [
                'name' => 'sok dara',
                'email' => 'dara@gmail.com',
                'address' => 'phnom penh'
            ],
            [
                'name' => 'pen morl',
                'email' => 'morl@gmail.com',
                'address' => 'phnom penh'
            ],
            [
                'name' => 'penh chomroren',
                'email' => 'chomroren@gmail.com',
                'address' => 'phnom penh'
            ]
        );
        //return dd($views);
        // return view('view',compact('name','views')); 
        return view('view',['views'=>$views]);
    }
    public function post(Request $resquest)
    {
        $name = $resquest->input('name');
        $date = $resquest->input('date');
        $address = $resquest->input('address');
        $data = array(
            'name'=>$name,
            'date'=>$date,
            'address'=>$address
        );
        //return dd($data);
        return view('welcome',['data'=>$data]);
    }
}
