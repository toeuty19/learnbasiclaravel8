<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ClientController extends Controller
{
    //http://127.0.0.1:8001
    public function getAllClient()
    {
        $data = Http::get('https://jsonplaceholder.typicode.com/posts');
        return $data->json();
    }
    function getById($id)
    {
        $data = Http::get('https://jsonplaceholder.typicode.com/posts/'.$id);
        return $data->json();
    }
    public function addPost()
    {
        $data = Http::post('https://jsonplaceholder.typicode.com/posts/',[
            'userId'=>1,
            'title'=>'hello world',
            'body'=>'this test to post'
        ]);
        return $data->json();
    }
    public function update($id)
    {
        $data = Http::put('https://jsonplaceholder.typicode.com/posts/'.$id,[
            'title'=>'update new tittle',
            'body'=>'update new description'
        ]);
        return 'data has been update successfully.';
    }
    public function delete($id=null)
    {
        $data = Http::delete('https://jsonplaceholder.typicode.com/posts/'.$id);
        return $data->json();
        //return 'data has been deleted successfully.';
    }
}
