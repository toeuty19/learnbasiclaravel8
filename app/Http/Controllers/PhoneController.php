<?php

namespace App\Http\Controllers;

use App\Models\phone;
use App\Models\User;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    //
    public function insertRecord()
    {
        $phone = new phone();
        $user = new User();
        $phone->phone = '1298983777';
        $user->name = 'Toeu Ty';
        $user->email = 'toeuty@gmail.com';
        $user->password = encrypt('secret');
        $user->save();
        $user->phone()->save($phone);

        return 'Record has been created successufully.';
    }

    public function FetchPhoneByUser($id)
    {
        $phone = User::find($id)->phone;
        return $phone;
    }
}
