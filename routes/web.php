<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\CurdModelController;
use App\Http\Controllers\FluentStringController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImageCurdController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PhoneController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\ViewController;
use App\Models\CurdModel;
use App\Models\ImageCurd;
use App\Models\Post;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//--------------learn contorller -----------------//
//Route::get('home',[HomeController::class,'index'])->name('home.index');
Route::get('home/{name}',[HomeController::class,'index'])->name('home.inex');
Route::get('home/{name?}',[HomeController::class,'index'])->name('home.index');

//--------------learn view -----------------------//
// Route::get('view',function(){
//     return view('view');
// });
Route::get('view',[ViewController::class,'index'])->name('view.index');
Route::post('view',[ViewController::class, 'post'])->name('view.post');

//--------------learn component-------------------//
Route::get('/',[ProductController::class,'index'])->name('product.index');

//--------------learn client----------------------.
Route::get('post',[ClientController::class, 'getAllClient'])->name('post.getAllClient');
Route::get('post/{userId}', [ClientController::class, 'getById'])->name('post.getById');
Route::get('addposts',[ClientController::class, 'addPost'])->name('addposts.addpost');
Route::get('update-post/{id}',[ClientController::class,'update'])->name('update-post.update');
Route::get('delete-post/{id?}',[ClientController::class,'delete'])->name('delete-post.delete');

//---------------learn fluent string----------------.
Route::get('fluent-string',[FluentStringController::class,'index'])->name('string.index');

//----------------learn send request client --------.
Route::get('/login',[LoginController::class, 'index'])->name('login.index');
Route::post('login',[LoginController::class, 'Login'])->name('login.login');

//----------------learn middleware -----------------.
Route::get('login',[LoginController::class,'index'])->middleware('checkuser');

//----------------learn session---------------------.

Route::get('session/get',[SessionController::class, 'getSessionData'])->name('session.get');

Route::get('session/set', [SessionController::class, 'setSessionData'])->name('session.set');

Route::get('session/remove', [SessionController::class, 'deleteSession'])->name('session.remove');

//-----------------learn database -------------------.

Route::get('posts',[PostController::class, 'all_posts'])->name('posts.all_posts');
Route::get('add-posts',[PostController::class, 'add_post'])->name('add.post');

Route::post('post_submit',[PostController::class, 'add_post_submit'])->name('post.submit');
Route::get('view-edit/{id}',[PostController::class, 'post_edit'])->name('view.edit');

Route::get('post-deleted/{id}',[PostController::class, 'post_deleted'])->name('post.deleted');
Route::post('post-update',[PostController::class, 'post_update'])->name('post.update');

//----------------learn inner join table--------------.

Route::get('innerjoin_table',[PostController::class, 'innerjoin_table'])->name('inner.join');


//----------------learn model ------------------------.

Route::get('all_post_model',[PostController::class, 'all_post_model'])->name('post.model');

Route::get('create-one-many',[PhoneController::class, 'insertRecord'])->name('add.oneToMany');

Route::get('Fect-phone-byUser/{id}',[PhoneController::class, 'FetchPhoneByUser'])->name('Phone.FetchPhoneByUser');

//----------------- learn curd model -----------------.

Route::get('view-curd',[CurdModelController::class,'view_index']);

Route::post('add-curd',[CurdModelController::class,'add_new'])->name('add.curd'); //

Route::get('list-curd', [CurdModelController::class, 'list_curd']);

Route::get('edit/{id}',[CurdModelController::class,'edit']);

Route::post('update-curd', [CurdModelController::class, 'update'])->name('curd.update');

Route::get('deleted-curd/{id}',[CurdModelController::class, 'deletecurd']);

//-----------------------Learn curd --------------------------///

Route::get('view_curd_image',[ImageCurdController::class, 'view_curd_image']);

Route::post('add_new_image',[ImageCurdController::class, 'add_new_image'])->name('add.new_image');

Route::get('select_view_all_image',[ImageCurdController::class, 'select_view_all_image'])->middleware('auth');

Route::get('edit_image/{id}',[ImageCurdController::class,'edit']);

Route::post('update',[ImageCurdController::class, 'update'])->name('update_image');

Route::get('deleted-image/{id}', [ImageCurdController::class, 'deleted_image']);

//--------------------------auth ------------------------------------------------//

Route::group(['middleware'=>'auth'],function(){
    Route::get('/',function(){
        $data = Auth::user()->name;
        //return dd($data);
        return view('home.home',compact('data'));
    });
    Route::get('/home',function(){
        return view('welcome');
    });
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
