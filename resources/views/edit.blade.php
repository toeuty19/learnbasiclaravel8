<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>add-post</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container pt-5">
        <h3>update data</h3>
        <hr>
        <form action="{{Route('post.update')}}" method="POST">
            @csrf
            @if (Session::has('updated'))
                <div class="alert alert-success">{{Session::get('updated')}}</div>
            @endif
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="">Tille</label>
                        <input type="hidden" name="id" value="{{$datas->id}}">
                        <input type="text" name="tittle" class="form-control" value="{{$datas->tittle}}" placeholder="Enter Tittle">
                    </div>
                    <div class="form-group">
                        <label for="">Tille</label>
                        <textarea name="description" id="" class="form-control" rows="3" placeholder="Description">{{$datas->description}}</textarea>
                    </div>
                    <input type="submit" value="Submit" class="btn btn-primary">
                </div>
            </div>
        </form>
    </div>
</body>
</html>