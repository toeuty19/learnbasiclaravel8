<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>add-post</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container py-5">
        <h3 class="text-primary">How to learn laravel 8. This session will learn curd Eloquent > <a href="/list-curd">view list</a></h3>
        <hr>
        
        
        <div class="row">
            <div class="col-lg-6">
                <form action="{{Route('curd.update')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="hidden" name="id" value="{{$curd->id}}">
                        <input type="text" name="name" placeholder="Enter Name" value="{{$curd->name}}" class="form-control">
                        @error('name')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <select name="gender" class="form-control">
                            <option value="M" {{$curd->gender=='M' ? 'selected':''}}>Male</option>
                            <option value="F" {{$curd->gender=='F' ? 'selected':''}}>Female</option>
                        </select>
                        @error('gender')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="date">Date Of Birth</label>
                        <input type="date" name="date" class="form-control" value="{{$curd->dob}}">
                        @error('date')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea name="address" id="" class="form-control" placeholder="Address" rows="3">{{$curd->address}}</textarea>
                        @error('address')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                     <div class="form-group">
                        <label for="phone">Mobile</label>
                        <input type="tel" name="phone" placeholder="Enter Mobile" class="form-control" value="{{$curd->phone}}">
                        @error('phone')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                     <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" placeholder="Enter Email" class="form-control" value="{{$curd->email}}">
                         @error('email')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-outline-info mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>