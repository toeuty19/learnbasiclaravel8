<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>List all data curd</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" />
</head>
<body>
    <div class="container py-5">
        @if (Session::has('updated'))
            <div class="alert my-3 alert-success">{{Session::get('updated')}}</div>
        @endif
        @if (Session::has('create_curd'))
            <div class="alert alert-success">{{Session::get('create_curd')}}</div>
        @endif
         @if (Session::has('deleted'))
            <div class="alert alert-success">{{Session::get('deleted')}}</div>
        @endif
        <div class="">
            <h3 class="text-primary">List All curd</h3>
            <a href="view-curd" class="btn btn-outline-primary btn-sm">Add New</a>
        </div>

        <table class="table table-hover nowrap">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Date</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($curds as $curd)
                <tr>
                    <td>{{$curd->id}}</td>
                    <td>{{$curd->name}}</td>
                    <td>{{$curd->gender}}</td>
                    <td>{{$curd->dob}}</td>
                    <td>{{$curd->address}}</td>
                    <td>{{$curd->phone}}</td>
                    <td>{{$curd->email}}</td>
                    <td>
                        <a href="edit/{{$curd->id}}" class="text-info pr-3"><i class="fas fa-edit"></i></a>
                        <a href="deleted-curd/{{$curd->id}}" class="text-danger"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    @if (Session::has('updated'))
        <script>
            swal("Updated", "One record has been updated successed", "success");
        </script>
    @endif

    @if (Session::has('deleted'))
        <script>
            swal("Deleted", "One record has been deleted successed", "success");
        </script>
    @endif

    @if (Session::has('create_curd'))
        <script>
            swal("Deleted", "One record has been created successed", "success");
        </script>
    @endif 
    
    @if (Session::has('updated'))
        <script>
            toastr.success("One record has been updated successed");
        </script>
    @endif
</body>
</html>