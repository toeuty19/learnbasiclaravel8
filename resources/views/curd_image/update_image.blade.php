<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add new image</title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container pt-5">
        <h3>This session we will learn curd image...!</h3>
        <hr>
        @if (Session::has('created_image'))
            <div class="alert alert-success">{{Session::get('created_image')}}</div>
        @endif
        <div class="row">
            <div class="col-lg-6">
                <form action="{{Route('update_image')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="hidden" name="id" value="{{$curd->id}}">
                        <input type="text" class="form-control" value="{{$curd->name}}" placeholder="Enter Name" name="name">
                    </div>
                     <div class="form-group">
                        <label for="name">select file</label>
                        <input type="file" class="form-control" value="{{$curd->imageName}}" name="file">
                    </div>
                    <button type="submit" class="btn btn-outline-info mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>