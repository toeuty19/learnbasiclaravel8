<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>View all image</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container pt-5">
        @if (Session::has('updated_image'))
            <div class="alert alert-success">{{Session::get('updated_image')}}</div>
        @endif
        @if (Session::has('deleted_image'))
            <div class="alert alert-success">{{Session::get('deleted_image')}}</div>
        @endif
        <div class="row">
            <div class="col-lg-6">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($curds as $curd)
                            <tr>
                                <td>{{$curd->id}}</td>
                                <td>{{$curd->name}}</td>
                                <td><img src="{{asset('images')}}/{{$curd->imageName}}" width="50px" height="40px" alt=""></td>
                                <td>
                                    <a href="edit_image/{{$curd->id}}" class="text-info pr-3"><i class="fas fa-edit"></i></a>
                                    <a href="deleted-image/{{$curd->id}}" class="text-danger"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>