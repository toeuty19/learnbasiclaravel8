<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>database get all data</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    
    <div class="container">
        <h3>List data</h3><br>
    <hr>
    @if (Session::has('deleted'))
        <div class="alert alert-success">{{Session::get('deleted')}}</div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Tittle</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($datas as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->tittle}}</td>
                    <td>{{$item->description}}</td>
                    <td>
                        <a class="btn btn-sm btn-info" href="view/{{$item->id}}">view</a>
                        <a class="btn btn-sm btn-primary mx-2" href="view-edit/{{$item->id}}">edit</a>
                        <a class="btn btn-sm btn-danger" href="post-deleted/{{$item->id}}">delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    
</body>
</html>