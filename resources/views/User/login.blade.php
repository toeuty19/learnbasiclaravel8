<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>

    <form action="{{Route('login.login')}}" method="POST">
        @csrf
        <div class="container">
             <div class="row">
                 <div class="col-lg-5">
                      <div class="card">
                <div class="card-header">
                    <div class="card-title">Login</div>
                </div>
                <div class="card-body">
                     <div class="form-group">
                      <label for="">username</label>
                      <input type="text" name="email" class="form-control" placeholder="Enter username">
                      @error('email')
                          <span class="text-danger">{{$message}}</span>
                      @enderror
                  </div>
                   <div class="form-group">
                      <label for="">password</label>
                      <input type="text" name="password" class="form-control" placeholder="Enter username">
                      @error('password')
                          <span class="text-danger">{{$message}}</span>
                      @enderror
                  </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
              </div>
                 </div>
             </div>
        </div>
    </form>

</body>

</html>