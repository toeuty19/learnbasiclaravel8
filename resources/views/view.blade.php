<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="hello world">
    <title>Learn view</title>
</head>
<style>
    .box {
        width: 400px;
        border: 1px solid #000;
        padding: 0 20px;
    }
</style>

<body>
    <div class="box">
        <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa sequi, debitis perspiciatis non dolorum dolore temporibus dolores, dicta, eligendi quas magni. Hic nostrum impedit ea nisi nam. Quod, a minima.
        </p>
    </div>
    <h1>Wellcome to view 1 {{$views[1]['name']}}</h1>
    <hr><br>
    <form action="" method="post">
        @csrf
        <table>
            <tr>
                <td>Name</td>
                <td>:</td>
                <td><input type="text" name="name" placeholder="please enter name"></td>
            </tr>
            <tr>
                <td>date of birth</td>
                <td>:</td>
                <td><input type="date" name="date" placeholder="please enter date"></td>
            </tr>
            <tr>
                <td>address</td>
                <td>:</td>
                <td><input type="text" name="address" placeholder="please enter address"></td>
            </tr>
            <tr>
                <td><input type="submit" value="Submit"></td>
            </tr>
        </table>

    </form>
    @foreach ($views as $item)
    <h3>{{$item['name']}}</h3>
    <h3>{{$item['email']}}</h3>
    <h3>{{$item['address']}}</h3>
    <hr />
    @endforeach
</body>

</html>